<?php

ini_set("display_errors", 1);
ini_set("log_errors", 1);
error_reporting(E_ALL);

include '../vendor/autoload.php';

use Mittum\SDK\Entity\TransactionalEmail;
use Mittum\SDK\Entity\Contact;
use Mittum\SDK\Entity\Delivery;
use Mittum\SDK\Exception\MittumResponseErrorException;
use Mittum\SDK\Mittum;

//Id Usuario Mittum
$mittumUserId = 2;

//Entorno de Mittum que se utiliza
$environmentName = 'eu';

//Identificador de la campaña Transaccional
$campaignId = 418646;

//Cada peticion debe ser diferente
$primaryKey = 122;

//Dirección de email que enviamos el correo
$email = "test@test.com";

try {
    $mittumSDK = new Mittum($mittumUserId, $environmentName);

    $delivery = new Delivery($campaignId, $primaryKey);
    $contact = new Contact($email);

    $completeTransactionalEmail = new TransactionalEmail($delivery, $contact);

    $mittumSDK->sendTransactional($completeTransactionalEmail);

    echo 'Enviado correctamente'.PHP_EOL;

} catch (MittumResponseErrorException $requestError) {
    echo 'La petición ha fallado por ' . $requestError->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    //Some Error
    var_dump($e->getMessage());
}