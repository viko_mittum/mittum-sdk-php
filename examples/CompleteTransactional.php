<?php

ini_set("display_errors", 1);
ini_set("log_errors", 1);
error_reporting(E_ALL);

include '../vendor/autoload.php';

use Mittum\SDK\Entity\CompleteTransactionalEmail;
use Mittum\SDK\Entity\Contact;
use Mittum\SDK\Entity\Delivery;
use Mittum\SDK\Exception\MittumResponseErrorException;
use Mittum\SDK\Mittum;

//Id Usuario Mittum
$mittumUserId = 2;

//Entorno de Mittum que se utiliza
$environmentName = 'eu';

//Identificador de la campaña, si ponemos simepre el mismo, la lista de contactos será única
$campaignId = 2323;

//Cada peticion debe ser diferente
$primaryKey = 12;

//Asunto del email
$subject = 'Hola  Test Transaccional Completo';

//Cuerpo del email
$body = 'Este es mi Body <!--$pixel-->';

//Email sender con el que enviaremos el email
$sender = 'enrique.vazquez@ibrands.es';

//Dirección de email que enviamos el correo
$email = "test@test.com";

try {
    $mittumSDK = new Mittum($mittumUserId, $environmentName);

    $delivery = new Delivery($campaignId, $primaryKey, $subject, $body, $sender);
    $contact = new Contact($email);

    $completeTransactionalEmail = new CompleteTransactionalEmail($delivery, $contact);

    $mittumSDK->sendCompleteTransactional($completeTransactionalEmail);

    echo 'Enviado correctamente'.PHP_EOL;

} catch (MittumResponseErrorException $requestError) {
    echo 'La petición ha fallado por ' . $requestError->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    //Some Error
    var_dump($e->getMessage());
}