<?php

ini_set("display_errors", 1);
ini_set("log_errors", 1);
error_reporting(E_ALL);

include '../vendor/autoload.php';

use Mittum\SDK\Entity\Contact;
use Mittum\SDK\Exception\MittumResponseErrorException;
use Mittum\SDK\Mittum;

//Id Usuario Mittum
$mittumUserId = 2;

//Entorno de Mittum que se utiliza
$environmentName = 'eu';

//Email del Contacto
$email = "test@test.com";

//Campos de segmentación de la lista, Puede ir vacio
$personalizationFields = array(
    "nombre_campo_personalizacion" => "Valor Campo Personalizacion",
);

//Campos de segmentación de la lista, Puede ir vacio
$segmentationFields = array(
    "nombre_campo_segmentación" => "Valor Campo Segmentación",
);

//Identificador de la list
$contactListId = 1;

try {
    $mittumSDK = new Mittum($mittumUserId, $environmentName);

    $contact = new Contact($email, $personalizationFields, $segmentationFields);

    $mittumSDK->sendContactToContactList($contactListId, $contact);

    echo 'Añadido Correctamente'.PHP_EOL;

} catch (MittumResponseErrorException $requestError) {
    echo 'La petición ha fallado por ' . $requestError->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    //Some Error
    var_dump($e->getMessage());
}