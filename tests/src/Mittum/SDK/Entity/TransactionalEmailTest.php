<?php

use \Mittum\SDK\Entity\Contact;

class TransactionalEmailTest extends AbstractTest {

    public function setUp()
    {
        parent::setUp();
    }

    public function testGetCampaignData()
    {
        $contact = ContactTest::getContactWith2SegmentationAnd2Personalization();
        $delivery = DeliveryTest::getDeliveryForTransactional();

        $transactionalEmail = new \Mittum\SDK\Entity\TransactionalEmail($delivery, $contact);

        $info = $transactionalEmail->getCampaignData();

        $this->assertEquals(5, count($info));
        $this->assertArrayHasKey("email", $info);
        $this->assertArrayHasKey("PK", $info);
        $this->assertArrayHasKey("CID", $info);

    }


}