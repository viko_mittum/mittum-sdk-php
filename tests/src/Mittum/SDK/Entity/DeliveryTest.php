<?php

use \Mittum\SDK\Entity\Delivery;
use \Mittum\SDK\Exception\MittumWrongCampaignIdException;
use \Mittum\SDK\Exception\MittumWrongPrimaryKeyException;


class DeliveryTest extends AbstractTest
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testConstruct()
    {
        $campaignId = "12";
        $primaryKey = "10";

        $delivery = new Delivery($campaignId, $primaryKey);

        $this->assertEquals("Mittum\\SDK\\Entity\\Delivery", get_class($delivery));
    }

    public function testConstructCampaignIdNotInt()
    {
        $campaignId = "1w2";
        $primaryKey = "10";

        $this->expectException(MittumWrongCampaignIdException::class);
        new Delivery($campaignId, $primaryKey);

    }

    public function testConstructPrimaryKeyNotInt()
    {
        $campaignId = "12";
        $primaryKey = "1w0";

        $this->expectException(\Mittum\SDK\Exception\MittumWrongPrimaryKeyException::class);
        new Delivery($campaignId, $primaryKey);

    }


    public function testGetDeliveryForTransactional()
    {
        $campaignId = "12";
        $primaryKey = "10";

        $delivery =  new Delivery($campaignId, $primaryKey);
        $information = $delivery->getDeliveryForTransactional();

        $this->assertCount(2, $information);
        $this->assertArrayHasKey("CID", $information);
        $this->assertArrayHasKey("PK", $information);
        $this->assertEquals($campaignId, $information["CID"]);
        $this->assertEquals($primaryKey, $information["PK"]);

    }


    public function testGetDeliveryForCompleteTransactional()
    {
        $campaignId = "12";
        $primaryKey = "10";
        $subject = 'Hola  Test Transaccional Completo';
        $body = 'Este es mi Body <!--$pixel-->';
        $sender = 'enrique.vazquez@ibrands.es';

        $delivery =  new Delivery($campaignId, $primaryKey, $subject, $body, $sender);
        $information = $delivery->getDeliveryForCompleteTransactional();

        $this->assertCount(5, $information);
        $this->assertArrayHasKey("CID", $information);
        $this->assertArrayHasKey("PK", $information);
        $this->assertArrayHasKey("remitente", $information);
        $this->assertArrayHasKey("asunto", $information);
        $this->assertArrayHasKey("cuerpo", $information);
        $this->assertEquals($campaignId, $information["CID"]);
        $this->assertEquals($primaryKey, $information["PK"]);
        $this->assertEquals($sender, $information["remitente"]);
        $this->assertEquals($body, $information["cuerpo"]);
        $this->assertEquals($subject, $information["asunto"]);

    }

    public static function getDeliveryForTransactional()
    {
        $campaignId = 24;
        $primaryKey = 32;
        return new Delivery($campaignId, $primaryKey);
    }

    public static function getDeliveryForCompleteTransactional()
    {
        $campaignId = 24;
        $primaryKey = 32;
        $subject = 'Hola  Test Transaccional Completo';
        $body = 'Este es mi Body <!--$pixel-->';
        $sender = 'test@test.es';
        return new Delivery($campaignId, $primaryKey, $subject, $body, $sender);
    }

}