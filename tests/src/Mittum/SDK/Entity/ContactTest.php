<?php

use \Mittum\SDK\Entity\Contact;

class ContactTest extends AbstractTest
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testContactBadEmail()
    {
        $email = "testest.com";
        $this->expectException(\Mittum\SDK\Exception\MittumWrongEmailException::class);
        $contact = new Contact($email);
    }

    public function testBadField()
    {
        $email = "test@test.com";
        $personalitzationFields = array("tes t" => "El valor que sea");
        $this->expectException(\Mittum\SDK\Exception\MittumWrongFieldException::class);
        $contact = new Contact($email, $personalitzationFields);
    }

    public function testGetContactInfoForTransaccional()
    {
        $email = "test@test.com";
        $segmentations = array(
            "country" => "spain"
        );
        $personalization = array(
            "name" => "Mittum"
        );
        $contact = new Contact($email, $personalization, $segmentations);

        $info = $contact->getContactInfoForTransaccional();

        $this->assertEquals(2, count($info));
        $this->assertArrayHasKey("email", $info);
        $this->assertArrayHasKey("country", $info);

    }

    public function testContactWithSegmentationAndPersonalization()
    {
        $email = "test@test.com";
        $contact = new Contact($email);
        $segmentations = array(
            "country" => "spain"
        );
        $contact->setSegmentationFields($segmentations);

        $personalization = array(
            "name" => "Mittum"
        );
        $contact->setPersonalizationFields($personalization);

        $info = $contact->getInfoInArrayWithFieldsPrefixed();

        $this->assertEquals(3, count($info));
        $this->assertArrayHasKey("email", $info);
        $this->assertArrayHasKey("s_country", $info);
        $this->assertArrayHasKey("p_name", $info);

    }


    public function testContactWithSegmentationAndPersonalizationWithSameFieldname()
    {
        $email = "test@test.com";
        $contact = new Contact($email);
        $segmentations = array(
            "name" => "spain"
        );
        $contact->setSegmentationFields($segmentations);

        $personalization = array(
            "name" => "Mittum"
        );
        $contact->setPersonalizationFields($personalization);

        $info = $contact->getInfoInArrayWithFieldsPrefixed();

        $this->assertEquals(3, count($info));
        $this->assertArrayHasKey("email", $info);
        $this->assertArrayHasKey("s_name", $info);
        $this->assertArrayHasKey("p_name", $info);

    }


    public static function getContactWith2SegmentationAnd2Personalization()
    {
        $email = "test@test.com";
        $personalizationFields = array(
            "field1" => "campo 1",
            "nombre" => "Test"
        );
        $segmentationFields = array(
            "field2" => "Campo 2",
            "Ciudad" => "Paris"
        );
        return new Contact($email, $personalizationFields, $segmentationFields);
    }


}