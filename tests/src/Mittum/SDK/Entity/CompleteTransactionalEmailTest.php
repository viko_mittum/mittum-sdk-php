<?php

use \Mittum\SDK\Entity\Contact;

class CompleteTransactionalEmailTest extends AbstractTest {

    public function setUp()
    {
        parent::setUp();
    }

    public function testGetCampaignData()
    {
        $contact = ContactTest::getContactWith2SegmentationAnd2Personalization();
        $delivery = DeliveryTest::getDeliveryForCompleteTransactional();

        $completeTransactionalEmail = new \Mittum\SDK\Entity\CompleteTransactionalEmail($delivery, $contact);

        $info = $completeTransactionalEmail->getCampaignData();

        $this->assertEquals(6, count($info));
        $this->assertArrayHasKey("email", $info);
        $this->assertArrayHasKey("PK", $info);
        $this->assertArrayHasKey("CID", $info);
        $this->assertArrayHasKey("remitente", $info);
        $this->assertArrayHasKey("asunto", $info);
        $this->assertArrayHasKey("cuerpo", $info);
    }


}