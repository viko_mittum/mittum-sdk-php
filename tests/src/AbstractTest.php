<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 13/1/17
 * Time: 13:35
 */

abstract class AbstractTest extends PHPUnit_Framework_TestCase {

    protected static function getAccesibleMethod( $className, $methodName) {
        $class = new ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }

}