<?php
namespace Mittum\SDK;

use Mittum\SDK\Entity\Campaign;
use Mittum\SDK\Entity\CompleteTransactionalEmail;
use Mittum\SDK\Entity\Email;
use Mittum\SDK\Entity\TransactionalEmail;
use Mittum\SDK\Environment\EnvironmentInterface;
use Mittum\SDK\Exception\MittumEnvironmentNotExistException;
use Mittum\SDK\Response\AddContactResponse;
use Mittum\SDK\Response\TransaccionalCompleteResponse;
use Mittum\SDK\Response\TransaccionalResponse;
use Mittum\SDK\Entity\Contact;


class Mittum extends MittumAbstract
{
    protected $userId;
    /** @var EnvironmentInterface */
    protected $environment;

    public function __construct($userId, $environmentName)
    {
        $this->userId = $userId;
        $this->environment = $this->createEnvironment($environmentName);
    }

    private function createEnvironment($environmentName)
    {
        if( ! (strlen($environmentName) > 0 && strlen($environmentName)<4) ){
            throw new MittumEnvironmentNotExistException("Environment not has valid Name");
        }

        $environmentClass = '\\Mittum\\SDK\\Environment\\' . strtoupper($environmentName);
        
        if(! class_exists($environmentClass)){
            throw new MittumEnvironmentNotExistException();
        }

        return new $environmentClass();
        
    }

    public function sendTransactional(TransactionalEmail $transactionalEmail)
    {
        $responseString = $this->doRequest(
            $this->environment->getTransactionalEndpoint(),
            $transactionalEmail->getCampaignData(),
            $this->userId
        );

        $response = new TransaccionalResponse($responseString);

        return $response;
    }

    public function sendCompleteTransactional(CompleteTransactionalEmail $completeTransactionalEmail)
    {
        $responseString = $this->doRequest(
            $this->environment->getCompleteTransactionalEndpoint(),
            $completeTransactionalEmail->getCampaignData(),
            $this->userId
        );

        $response = new TransaccionalCompleteResponse($responseString);

        return $response;
    }

    public function sendContactToContactList($contactListId, Contact $contact)
    {
        $infoToSend = $contact->getInfoInArrayWithFieldsPrefixed();
        $infoToSend["CLID"] = $contactListId;

        $responseString = $this->doRequest(
            $this->environment->getAddContactEndpoint(),
            $infoToSend
        );

        $response = new AddContactResponse($responseString);
        return $response;

    }

}








