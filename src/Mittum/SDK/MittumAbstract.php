<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 10:56
 */
namespace Mittum\SDK;


use Mittum\SDK\Exception\MittumInternalErrorException;

abstract class MittumAbstract
{
    protected function doRequest($endpoint, array $fields, $userId = null)
    {
        if($userId !== null) {
            $fields["userid"] = $userId;
        }

        $fields_string= http_build_query($fields);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $endpoint);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if($result === false){
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        curl_close($ch);
        return $result;
    }

}