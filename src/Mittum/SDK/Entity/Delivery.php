<?php
namespace Mittum\SDK\Entity;

use Mittum\SDK\Exception\MittumWrongCampaignIdException;
use Mittum\SDK\Exception\MittumWrongPrimaryKeyException;

class Delivery
{
    private $sender;
    private $campaignId;
    private $subject;
    private $body;
    private $primaryKey;

    public function __construct($campaignId, $primaryKey, $subject = null, $body = null, $sender = null)
    {
        $this->setCampaignId($campaignId);
        $this->setPrimaryKey($primaryKey);
        $this->subject = $subject;
        $this->body = $body;
        $this->sender = $sender;
    }

    private function setCampaignId($campaignId)
    {
        $campaignId = filter_var($campaignId, FILTER_VALIDATE_INT);

        if ($campaignId === false) {
            throw new MittumWrongCampaignIdException();
        }
        $this->campaignId = $campaignId;
    }

    private function setPrimaryKey($primaryKey)
    {
        $primaryKey = filter_var($primaryKey, FILTER_VALIDATE_INT);
        if ($primaryKey === false) {
            throw new MittumWrongPrimaryKeyException();
        }
        $this->primaryKey = $primaryKey;
    }

    public function getDeliveryForCompleteTransactional()
    {
        return array(
            "PK" => $this->primaryKey,
            "magic" => "mag1c",
            "remitente" => $this->sender,
            "asunto" => $this->subject,
            "cuerpo" => $this->body,
            "CID" => $this->campaignId
        );
    }

    public function getDeliveryForTransactional()
    {
        return array(
            "PK" => $this->primaryKey,
            "magic" => "mag1c",
            "CID" => $this->campaignId
        );
    }

}