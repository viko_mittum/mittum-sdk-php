<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 3/4/17
 * Time: 11:34
 */

namespace Mittum\SDK\Entity;

interface CampaignInterface {

    public function getCampaignData();

}