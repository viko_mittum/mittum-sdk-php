<?php
namespace Mittum\SDK\Entity;

use Mittum\SDK\Exception\MittumWrongEmailException;
use Mittum\SDK\Exception\MittumWrongFieldException;

class Contact
{
    private $email;
    private $segmentationFields = array();
    private $personalizationFields = array();

    public function __construct($email, array $personalizationFields = array(), array $segmentationFields = array())
    {
        $this->setEmail($email);
        $this->setSegmentationFields($segmentationFields);
        $this->setPersonalizationFields($personalizationFields);
    }

    private function setEmail($email)
    {
        $email = trim($email);
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if ($email === false) {
            throw new MittumWrongEmailException();
        }
        $this->email = $email;
    }

    public function addSegmentationField($segmentationField, $segmentationValue)
    {
        $segmentationField = $this->validateFieldName($segmentationField);
        $this->segmentationFields[$segmentationField] = $segmentationValue;
        return $this;
    }

    private function validateFieldName($fieldName)
    {
        $fieldName = trim($fieldName);
        $options = array(
            "options" => array(
                "regexp" => '/^([a-z0-9_-])+$/i'
            )
        );

        $fieldName = filter_var($fieldName, FILTER_VALIDATE_REGEXP, $options);

        if ($fieldName === false) {
            throw new MittumWrongFieldException();
        }
        return $fieldName;
    }

    public function setSegmentationFields($segmentationFields)
    {
        $this->segmentationFields = array();
        foreach ($segmentationFields as $segmentationField => $segmentationValue) {
            $this->addSegmentationField($segmentationField, $segmentationValue);
        }
        return $this;
    }

    public function addPersonalizationField($personalizationField, $personalizationValue)
    {
        $personalizationField = $this->validateFieldName($personalizationField);
        $this->personalizationFields[$personalizationField] = $personalizationValue;
        return $this;
    }

    public function setPersonalizationFields($personalizationFields)
    {
        $this->personalizationFields = array();
        foreach ($personalizationFields as $personalizationField => $personalizationValue) {
            $this->addPersonalizationField($personalizationField, $personalizationValue);
        }
        return $this;
    }

    public function getInfoInArrayWithFieldsPrefixed()
    {
        $info = array(
            "email" => $this->email
        );

        $info = array_merge($info, $this->getSegmentationFieldsForArray());
        $info = array_merge($info, $this->getPersonalizationFieldsForArray());

        return $info;
    }

    public function getContactInfoForTransaccional()
    {
        $info = array(
            "email" => $this->getEmail()
        );

        $info = array_merge($info, $this->segmentationFields);

        return $info;
    }

    private function getSegmentationFieldsForArray()
    {
        $segmentationFieldsArray = array();
        foreach ($this->segmentationFields as $segmentationField => $segmentationValue) {
            $segmentationFieldsArray["s_" . $segmentationField] = $segmentationValue;
        }
        return $segmentationFieldsArray;
    }

    private function getPersonalizationFieldsForArray()
    {
        $personalizationFieldsArray = array();
        foreach ($this->personalizationFields as $personalizationField => $personalizationValue) {
            $personalizationFieldsArray["p_" . $personalizationField] = $personalizationValue;
        }
        return $personalizationFieldsArray;
    }

    public function getEmail()
    {
        return $this->email;
    }

}