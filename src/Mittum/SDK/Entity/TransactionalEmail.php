<?php
namespace Mittum\SDK\Entity;

class TransactionalEmail implements CampaignInterface
{
    private $contact;
    private $delivery;

    public function __construct(Delivery $delivery, Contact $contact)
    {
        $this->delivery = $delivery;
        $this->contact = $contact;
    }

    public function getCampaignData()
    {
        $campaign = array_merge(
            $this->delivery->getDeliveryForTransactional(),
            $this->contact->getContactInfoForTransaccional()
        );

        return $campaign;
    }


}