<?php
namespace Mittum\SDK\Entity;

use Mittum\SDK\Exception\MittumWrongCampaignIdException;
use Mittum\SDK\Exception\MittumWrongPrimaryKeyException;

class CompleteTransactionalEmail implements CampaignInterface
{
    private $delivery;
    private $contact;

    public function __construct(Delivery $delivery, Contact $contact)
    {
        $this->delivery = $delivery;
        $this->contact = $contact;
    }

    public function getCampaignData()
    {
        $campaign = array_merge(
            $this->delivery->getDeliveryForCompleteTransactional(),
            array("email" => $this->contact->getEmail())
        );

        return $campaign;
    }
}