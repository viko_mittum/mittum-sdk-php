<?php

namespace Mittum\SDK\Exception;

use Exception;

class MittumInternalErrorException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 )? "Mittum Internal Error" : $message;
        parent::__construct($message, $code, $previous);
    }
}

