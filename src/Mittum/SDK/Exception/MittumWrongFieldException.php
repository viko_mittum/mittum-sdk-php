<?php

namespace Mittum\SDK\Exception;

use Exception;

class MittumWrongFieldException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 )? "The fieldname can only be Alphanumeric" : $message;
        parent::__construct($message, $code, $previous);
    }
}

