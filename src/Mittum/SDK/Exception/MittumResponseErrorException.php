<?php

namespace Mittum\SDK\Exception;

class MittumResponseErrorException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 )? "Mittum Response Error" : $message;
        parent::__construct($message, $code, $previous);
    }
}

