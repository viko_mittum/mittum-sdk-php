<?php

namespace Mittum\SDK\Exception;

use Exception;

class MittumWrongPrimaryKeyException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 )? "Wrong Primary Key must be integer" : $message;
        parent::__construct($message, $code, $previous);
    }
}

