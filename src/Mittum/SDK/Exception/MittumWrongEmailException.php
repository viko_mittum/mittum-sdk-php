<?php

namespace Mittum\SDK\Exception;

use Exception;

class MittumWrongEmailException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 )? "Wrong Email" : $message;
        parent::__construct($message, $code, $previous);
    }
}

