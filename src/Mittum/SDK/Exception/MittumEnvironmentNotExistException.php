<?php

namespace Mittum\SDK\Exception;

use Exception;

class MittumEnvironmentNotExistException extends \Exception
{
    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 ) ? "Environment Not Exist" : $message;
        parent::__construct($message, $code, $previous);
    }
}

