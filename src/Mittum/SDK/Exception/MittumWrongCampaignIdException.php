<?php

namespace Mittum\SDK\Exception;

use Exception;

class MittumWrongCampaignIdException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $message = (strlen($message) == 0 )? "Wrong Campaign Id must be integer" : $message;
        parent::__construct($message, $code, $previous);
    }
}

