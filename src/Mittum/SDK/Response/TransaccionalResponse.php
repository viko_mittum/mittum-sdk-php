<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 11:01
 */

namespace Mittum\SDK\Response;

use Mittum\SDK\Exception\MittumInternalErrorException;
use Mittum\SDK\Exception\MittumResponseErrorException;

class TransaccionalResponse extends ResponseAbstract
{
    /*
     * 1: Envío realizado correctamente.
     * -1: Falta el parámetro obligatorio CID
     * -2: Falta el parámetro obligatorio email
     * -3: Falta el parámetro obligatorio PK (si es vacío debe existir y ser igual a NULL)
     * -4: La sintaxis del email es incorrecta
     * -5: Error de sistema (contactar con el servicio técnico de Elogia),
     * -6: El parámetro CID no corresponde a ningún evento transaccional.
     * -7: Falló la autorización por IP
     * -8: La dirección de email aparece en la blacklist de MITTUM.
     * -9: La dirección de email aparece como HardBounce (Badmail) en MITTUM.
     * -10: El mensaje contiene variables de personalización no definidas en la petición.
     * -11: Los ficheros adjuntos superan el tamaño máximo de 2MB.
     * -12: La petición contiene variables de personalización no presentes en peticiones anteriores
     */

    CONST CODE_OK = 1;
    CONST CODE_FAIL_NOT_CID = -1;
    CONST CODE_FAIL_NOT_EMAIL = -2;
    CONST CODE_FAIL_NOT_PK = -3;
    CONST CODE_FAIL_NOT_BAD_EMAIL = -4;
    CONST CODE_FAIL_INTERNAL_ERROR = -5;
    CONST CODE_FAIL_CID_NOT_TRANSACCIONAL = -6;
    CONST CODE_FAIL_IP_NOT_ACCEPTED = -7;
    CONST CODE_FAIL_EMAIL_IN_MITTUM_BLACKLIST = -8;
    CONST CODE_FAIL_EMAIL_IN_MITTUM_HARDBOUNCE = -9;
    CONST CODE_FAIL_NOT_ALL_VARIABLES_SETTER = -10;
    CONST CODE_FAIL_ATTACHMENTS_FILES_MORE_THAN_2MB = -11;
    CONST CODE_FAIL_ARE_MORE_VARIABLES_THAN_ANOTHER = -12;

    CONST STATUS_SUCCESS = "OK";
    CONST STATUS_ERROR = "ERROR";


    protected function setCode($responseObject)
    {
        $state_code = $responseObject->state_code->__toString();
        if (!(
            $state_code >= -12 &&
            $state_code <= 1 &&
            $state_code != 0
        )
        ) {
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        $this->code = $state_code;

        return $this;
    }

    public function isSuccess()
    {
        if ($this->isSuccessState()) {
            if ($this->code == 1) {
                return true;
            }
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        return false;
    }

    protected function setStatus( $responseObject)
    {
        $this->status = $responseObject->state->__toString();

        if ($this->status == self::STATUS_SUCCESS){
            $this->success = self::SUCCESS_RESPONSE;
        }
        return $this;
    }

    protected function parseErrorCode()
    {
        switch ($this->code) {
            case self::CODE_OK:
                return;
                break;
            case self::CODE_FAIL_NOT_CID:
                $message = "Falta el parámetro obligatorio CID";
                break;
            case self::CODE_FAIL_NOT_EMAIL:
                $message = "Falta el parámetro obligatorio email";
                break;
            case self::CODE_FAIL_NOT_PK:
                $message = "Falta el parámetro obligatorio PK (si es vacío debe existir y ser igual a NULL)";
                break;
            case self::CODE_FAIL_NOT_BAD_EMAIL:
                $message = "La sintaxis del email es incorrecta";
                break;
            case self::CODE_FAIL_INTERNAL_ERROR:
                $message = "Error de sistema (contactar con el servicio técnico de VIKO)";
                break;
            case self::CODE_FAIL_CID_NOT_TRANSACCIONAL:
                $message = "El parámetro CID no corresponde a ningún evento transaccional.";
                break;
            case self::CODE_FAIL_IP_NOT_ACCEPTED:
                $message = "Falló la autorización por IP";
                break;
            case self::CODE_FAIL_EMAIL_IN_MITTUM_BLACKLIST:
                $message = "La dirección de email aparece en la blacklist de MITTUM.";
                break;
            case self::CODE_FAIL_EMAIL_IN_MITTUM_HARDBOUNCE:
                $message = "La dirección de email aparece como HardBounce (Badmail) en MITTUM.";
                break;
            case self::CODE_FAIL_NOT_ALL_VARIABLES_SETTER:
                $message = "El mensaje contiene variables de personalización no definidas en la petición.";
                break;
            case self::CODE_FAIL_ATTACHMENTS_FILES_MORE_THAN_2MB:
                $message = "Los ficheros adjuntos superan el tamaño máximo de 2MB.";
                break;
            case self::CODE_FAIL_ARE_MORE_VARIABLES_THAN_ANOTHER:
                $message = "La petición contiene variables de personalización no presentes en peticiones anteriores";
                break;
            default:
                $message = "NOT DEFINED";
                break;
        }
        throw new MittumResponseErrorException($message, $this->code);
    }

    protected function getStatusError()
    {

        return self::STATUS_ERROR;
    }

    protected function getStatusSuccess()
    {
        return self::STATUS_SUCCESS;
    }
}