<?php

/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 10:40
 */

namespace Mittum\SDK\Response;


abstract class ResponseAbstract
{
    protected $status;
    protected $success = self::ERROR_RESPONSE;
    protected $code;
    protected $mittumPrimaryKey;

    CONST ERROR_RESPONSE = false;
    CONST SUCCESS_RESPONSE = true;

    public function __construct($responseString)
    {
        $xml = simplexml_load_string($responseString);
        $this->setCode($xml);
        $this->setStatus($xml);

        if ($this->success == self::ERROR_RESPONSE) {
            $this->parseErrorCode();
        }
    }

    protected function setErrorResponse()
    {
        $this->success = self::ERROR_RESPONSE;
    }

    protected function setSuccessResponse()
    {
        $this->success = self::SUCCESS_RESPONSE;
    }

    protected function isSuccessState()
    {
        return $this->success;
    }

    abstract protected function setStatus($responseObject);

    abstract protected function setCode($responseObject);

    abstract protected function parseErrorCode();

    abstract protected function getStatusError();

    abstract protected function getStatusSuccess();
}