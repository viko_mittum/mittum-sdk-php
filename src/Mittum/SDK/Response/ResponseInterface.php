<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 11:00
 */

namespace Mittum\SDK\Response;


interface ResponseInterface
{
    public function isSuccess();
}