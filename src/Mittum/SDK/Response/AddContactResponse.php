<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 11:01
 */

namespace Mittum\SDK\Response;

use Mittum\SDK\Exception\MittumInternalErrorException;
use Mittum\SDK\Exception\MittumResponseErrorException;

class AddContactResponse extends ResponseAbstract
{
    /*
     * 1: Lista de Contactos creada correctamente.
     * -1: Falta el parámetro obligatorio CLID
     * -2: Falta el parámetro obligatorio email
     * -5: El parámetro CLID no tiene un formato numérico adecaudo.
     * -6: La CLID no existe.
     * -7: La IP que realiza la petición no esta autorizada para consultar esta lista de contactos
     * -12: Los campos s_ y p_ de la petición no coinciden con los de la lista de contactos especificada con el CLID
     * -13: El email ya existe en la CLID
     * */

    CONST CODE_OK = 1;
    CONST CODE_FAIL_NOT_CLID = -1;
    CONST CODE_FAIL_NOT_EMAIL = -2;
    CONST CODE_FAIL_BAD_CLID = -5;
    CONST CODE_FAIL_CID_NOT_EXISTS = -6;
    CONST CODE_FAIL_IP_NOT_ACCEPTED = -7;
    CONST CODE_FAIL_ARE_MORE_VARIABLES_THAN_ANOTHER = -12;
    CONST CODE_FAIL_EMAIL_EXISTS = -13;

    CONST STATUS_SUCCESS = "OK";
    CONST STATUS_ERROR = "ERROR";


    protected function setCode($responseObject)
    {
        $state_code = $responseObject->state_code->__toString();
        if (!(
            $state_code >= -13 &&
            $state_code <= 1 &&
            $state_code != 0
        )
        ) {
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        $this->code = $state_code;

        return $this;
    }

    public function isSuccess()
    {
        if ($this->isSuccessState()) {
            if ($this->code == 1) {
                return true;
            }
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        return false;
    }

    protected function setStatus( $responseObject)
    {
        $this->status = $responseObject->state->__toString();

        if ($this->status == self::STATUS_SUCCESS){
            $this->success = self::SUCCESS_RESPONSE;
        }

        return $this;
    }

    protected function parseErrorCode()
    {
        switch ($this->code) {
            case self::CODE_OK:
                return;
                break;
            case self::CODE_FAIL_NOT_CLID:
                $message = "Falta el parámetro obligatorio CLID.";
                break;
            case self::CODE_FAIL_NOT_EMAIL:
                $message = "Falta el parámetro obligatorio email.";
                break;
            case self::CODE_FAIL_BAD_CLID:
                $message = "El parámetro CLID no tiene un formato numérico adecaudo.";
                break;
            case self::CODE_FAIL_CID_NOT_EXISTS:
                $message = "La CLID no existe.";
                break;
            case self::CODE_FAIL_IP_NOT_ACCEPTED:
                $message = "Falló la autorización por IP.";
                break;
            case self::CODE_FAIL_ARE_MORE_VARIABLES_THAN_ANOTHER:
                $message = "Los campos s_ y p_ de la petición no coinciden con los de la lista de contactos especificada con el CLID";
                break;
            case self::CODE_FAIL_EMAIL_EXISTS:
                $message = "El email ya existe en la CLID";
                break;
            default:
                $message = "NOT DEFINED";
                break;
        }
        throw new MittumResponseErrorException($message, $this->code);
    }

    protected function getStatusError()
    {

        return self::STATUS_ERROR;
    }

    protected function getStatusSuccess()
    {
        return self::STATUS_SUCCESS;
    }
}