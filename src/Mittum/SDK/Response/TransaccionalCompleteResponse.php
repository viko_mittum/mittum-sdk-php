<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 11:01
 */

namespace Mittum\SDK\Response;


use Mittum\SDK\Exception\MittumInternalErrorException;
use Mittum\SDK\Exception\MittumResponseErrorException;

class TransaccionalCompleteResponse extends ResponseAbstract implements ResponseInterface
{
    /*
     * 1 : Envío realizado correctamente
     * -1: Falta el parámetro obligatorio CID
     * -2: Falta el parámetro obligatorio remitente
     * -3: Falta el parámetro obligatorio email
     * -4: Falta el parámetro obligatorio asunto
     * -5: Falta el parámetro obligatorio cuerpo
     * -6: Falta el elemento <!--$pixel--> en el cuerpo HTML
     * -7: La sintaxis del remitente es incorrecta
     * -8: La sintaxis del email es incorrecta
     * -9: La longitud del asunto supera los 250 caracteres
     * -10: La longitud del cuerpo supera los 500K caracteres
     * -11: Ficheros adjuntos superan el tampao maximo de 2Mb
     * -12: Sintaxis CID incorrecta
     * -13: Limite de 20 archivos superado
     * -14: Falta el parametro obligatorio userid
     * -15: IP incorrecta para el userid
     * -100: Error de sistema (consultar con el servicio técnico de Elogia)
     */

    CONST CODE_OK = 1;
    CONST CODE_FAIL_NOT_CID = -1;
    CONST CODE_FAIL_NOT_SENDER = -2;
    CONST CODE_FAIL_NOT_EMAIL = -3;
    CONST CODE_FAIL_NOT_SUBJECT = -4;
    CONST CODE_FAIL_NOT_HTML = -5;
    CONST CODE_FAIL_NOT_PIXEL_IN_HTML = -6;
    CONST CODE_FAIL_BAD_SENDER = -7;
    CONST CODE_FAIL_BAD_EMAIL = -8;
    CONST CODE_FAIL_SUBJECT_MAX_250_CHARACTERS = -9;
    CONST CODE_FAIL_BODY_MAX_500K_CHARACTERS = -10;
    CONST CODE_FAIL_ATTACHMENTS_FILES_MORE_THAN_2MB = -11;
    CONST CODE_FAIL_BAD_CID = -12;
    CONST CODE_FAIL_ATTACHMENTS_MORE_THAN_20_FILES = -13;
    CONST CODE_FAIL_NOT_USER_ID = -14;
    CONST CODE_FAIL_IP_NOT_ACCEPTED = -15;
    CONST CODE_FAIL_INTERNAL_ERROR = -100;

    CONST STATUS_SUCCESS = "OK";
    CONST STATUS_ERROR = "ERROR";


    protected function setCode($responseObject)
    {
        $state_code = $responseObject->state_code->__toString();

        if (!(
            $state_code >= -100 &&
            $state_code <= 1 &&
            $state_code != 0
        )
        ) {
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        $this->code = $state_code;

        return $this;
    }

    public function isSuccess()
    {
        if ($this->isSuccessState()) {
            if ($this->code == 1) {
                return true;
            }
            throw new MittumInternalErrorException("BAD RESPONSE");
        }
        return false;
    }

    protected function parseErrorCode() {
        switch ($this->code) {
            case self::CODE_OK:
                return;
                break;
            case self::CODE_FAIL_NOT_CID:
                $message = "Falta el parámetro obligatorio CID";
                break;
            case self::CODE_FAIL_NOT_SENDER:
                $message = "Falta el parámetro obligatorio remitente";
                break;
            case self::CODE_FAIL_NOT_EMAIL:
                $message = "Falta el parámetro obligatorio email";
                break;
            case self::CODE_FAIL_NOT_SUBJECT:
                $message = "Falta el parámetro obligatorio asunto";
                break;
            case self::CODE_FAIL_NOT_HTML:
                $message = "Falta el parámetro obligatorio cuerpo";
                break;
            case self::CODE_FAIL_NOT_PIXEL_IN_HTML:
                $message = "Falta el elemento <!--\$pixel--> en el cuerpo HTML";
                break;
            case self::CODE_FAIL_BAD_SENDER:
                $message = "La sintaxis del remitente es incorrecta";
                break;
            case self::CODE_FAIL_BAD_EMAIL:
                $message = "La sintaxis del email es incorrecta";
                break;
            case self::CODE_FAIL_SUBJECT_MAX_250_CHARACTERS:
                $message = "La longitud del asunto supera los 250 caracteres";
                break;
            case self::CODE_FAIL_BODY_MAX_500K_CHARACTERS:
                $message = "La longitud del cuerpo supera los 500K caracteres.";
                break;
            case self::CODE_FAIL_ATTACHMENTS_FILES_MORE_THAN_2MB:
                $message = "Ficheros adjuntos superan el tampao maximo de 2Mb.";
                break;
            case self::CODE_FAIL_BAD_CID:
                $message = "Sintaxis CID incorrecta.";
                break;
            case self::CODE_FAIL_ATTACHMENTS_MORE_THAN_20_FILES:
                $message = "Limite de 20 archivos superado.";
                break;
            case self::CODE_FAIL_NOT_USER_ID:
                $message = "Falta el parametro obligatorio userid";
                break;
            case self::CODE_FAIL_IP_NOT_ACCEPTED:
                $message = "IP incorrecta para el userid";
                break;
            case self::CODE_FAIL_INTERNAL_ERROR:
                $message = "Error de sistema (consultar con el servicio técnico de VIKO)";
                break;
            default:
                $message = "CODE IS NOT DEFINED";
                break;
        }

        throw new MittumResponseErrorException($message, $this->code);
    }

    protected function setStatus( $responseObject)
    {
        $this->status = $responseObject->state->__toString();

        if ($this->status == self::STATUS_SUCCESS){
            $this->success = self::SUCCESS_RESPONSE;
        }
        return $this;
    }

    protected function getStatusError()
    {

        return self::STATUS_ERROR;
    }

    protected function getStatusSuccess()
    {
        return self::STATUS_SUCCESS;
    }
}