<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 10:42
 */

namespace Mittum\SDK\Environment;


class US extends EnvironmentAbstract implements EnvironmentInterface
{

    public function __construct()
    {
        $this->environmentUrl = "https://us.mittum.com";
    }

}