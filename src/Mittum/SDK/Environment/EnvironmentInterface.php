<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 10:41
 */

namespace Mittum\SDK\Environment;


interface EnvironmentInterface
{
    public function getTransactionalEndpoint();
    public function getTransaccionalStatusEndpoint();
    public function getAddContactEndpoint();
    public function getCompleteTransactionalEndpoint();
    public function getCompleteTransaccionalStatusEndpoint();
    public function getEnvironmentUrl();

}