<?php
/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 10:42
 */

namespace Mittum\SDK\Environment;

class EU extends EnvironmentAbstract implements EnvironmentInterface
{

    public function __construct()
    {
        $host= gethostname();
        $ip = gethostbyname($host);

        if (($host == 'mittumfrontend') && ($ip =='136.243.82.35')) {
          $this->environmentUrl = "http://eu.mittum.com";
        } else {
          $this->environmentUrl = "https://eu.mittum.com";
        }
    }

}



