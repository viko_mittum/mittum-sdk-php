<?php

/**
 * Created by PhpStorm.
 * User: nildorado
 * Date: 23/4/16
 * Time: 10:40
 */

namespace Mittum\SDK\Environment;


abstract class EnvironmentAbstract
{
    CONST TRANSACTIONAL_SEND_ENDPOINT = "/ws/e";
    CONST TRANSACTIONAL_STATUS_ENDPOINT = "/ws/s";
    CONST COMPLETE_TRANSACTIONAL_SEND_ENDPOINT = "/ws/ec";
    CONST COMPLETE_TRANSACTIONAL_STATUS_ENDPOINT = "/ws/sc";
    CONST COMPLETE_ADD_CONTACT_ENDPOINT = "/ws/c";

    protected $environmentUrl;

    public function getEnvironmentUrl()
    {
        return $this->environmentUrl;
    }

    public function getTransactionalEndpoint()
    {
        return $this->getEnvironmentUrl().self::TRANSACTIONAL_SEND_ENDPOINT;
    }

    public function getTransaccionalStatusEndpoint()
    {
        return $this->getEnvironmentUrl().self::TRANSACTIONAL_STATUS_ENDPOINT;
    }

    public function getCompleteTransactionalEndpoint()
    {
        return $this->getEnvironmentUrl().self::COMPLETE_TRANSACTIONAL_SEND_ENDPOINT;
    }

    public function getCompleteTransaccionalStatusEndpoint()
    {
        return $this->getEnvironmentUrl().self::COMPLETE_TRANSACTIONAL_STATUS_ENDPOINT;
    }

    public function getAddContactEndpoint()
    {
        return $this->getEnvironmentUrl().self::COMPLETE_ADD_CONTACT_ENDPOINT;
    }

}