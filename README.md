# MITTUM SDK PHP

Con Mittum SDK te ayudará a hacer peticiones a la API de MITTUM en PHP.

## Instalación

### Composer

Actualmente el SDK de Mittum es un repositorio privado, con lo cual debemos hacer lo siguiente:

#### Archivo composer.json

En el proyecto que queremos utilizar el SDK de MITTUM añadiremos:
  
```
"repositories": [  
     {
       "type": "vcs",
       "url":  "git@bitbucket.org:viko_mittum/mittum-sdk-php.git"
     }
]
```

y en el require:

```
"require": {
    ...
    "viko_mittum/mittum-sdk-php": "1.*",
    ...
  },
```

* NOTA:  
Los puntos suspensivos es por si tienes algo más en el require que lo mantengas, solo tiene que añadir ```"viko_mittum/mittum-sdk-php": "dev-master"```  

Acordaros también de que se tiene que añadir la clave de despliegue dentro del proyecto de Mittum-SDK.